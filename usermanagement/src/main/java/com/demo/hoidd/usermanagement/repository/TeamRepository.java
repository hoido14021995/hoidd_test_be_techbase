package com.demo.hoidd.usermanagement.repository;

import com.demo.hoidd.usermanagement.entity.TeamEntity;
import com.demo.hoidd.usermanagement.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface TeamRepository extends JpaRepository<TeamEntity, Long> {

    @Query("SELECT T FROM TeamEntity T WHERE T.roomID = :id")
    List<TeamEntity> findListTeamInRoom(@Param("id") Long id);
}
