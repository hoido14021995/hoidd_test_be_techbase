package com.demo.hoidd.usermanagement.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.RSAKeyProvider;
import com.demo.hoidd.usermanagement.dto.JWTTokenDTO;
import com.demo.hoidd.usermanagement.dto.UserDTO;
import com.demo.hoidd.usermanagement.utils.KeyReader;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Calendar;
import java.util.GregorianCalendar;

@Service
public class TokenServiceImpl implements TokenService {
    private final Logger LOGGER = LoggerFactory.getLogger(TokenServiceImpl.class);

    private static final String ISSUER = "ADMIN-SERVICE";
    private static final String TOKEN_TYPE = "WEB";
    private static final String CLAIM_USERNAME = "username";
    private static final String CLAIM_ROLE = "role";
    private static final String CLAIM_TOKEN_TYPE = "tokenType";

    private String privateKeyId = "keyId";

    private RSAKeyProvider mKeyProvider = new RSAKeyProvider() {
        @Override
        public RSAPublicKey getPublicKeyById(String kid) {
            // Received 'kid' value might be null if it wasn't defined in the
            // Token's header
            try {
                return KeyReader.getPublicKey("\\public_key.der");
            } catch (Exception e) {
                LOGGER.error("Get public key by Id fail with error: ", e);
            }
            return null;
        }


        @Override
        public RSAPrivateKey getPrivateKey() {
            try {
                return KeyReader.getPrivateKey("\\private_key.der");
            } catch (Exception e) {
                LOGGER.error("Get private key fail with error: ", e);
            }

            return null;
        }

        @Override
        public String getPrivateKeyId() {
            return privateKeyId;
        }
    };

    @Override
    public String generate(UserDTO userDTO) {
        Calendar currentDate = GregorianCalendar.getInstance();
        // Remove 30 seconds to avoid any failure between 2 VM
        currentDate.add(Calendar.SECOND, -30);
        Calendar expiryDate = GregorianCalendar.getInstance();
        expiryDate.add(Calendar.HOUR, 8);

        Algorithm algorithm = Algorithm.RSA256(null, mKeyProvider.getPrivateKey());
        return JWT.create().withIssuer(ISSUER).withClaim(CLAIM_USERNAME, userDTO.getUsername()).withClaim(CLAIM_ROLE, userDTO.getRole())
                .withClaim(CLAIM_TOKEN_TYPE, TOKEN_TYPE)
                .withExpiresAt(expiryDate.getTime()).sign(algorithm);
    }

    @Override
    public JWTTokenDTO extractToken(String token) {
        JWTTokenDTO tokenDTO = new JWTTokenDTO();
        try {
            if (StringUtils.isEmpty(token)) {
                return null;
            }
            Algorithm algorithm = Algorithm.RSA256(mKeyProvider.getPublicKeyById(""), null);
            JWTVerifier verifier = JWT.require(algorithm).withIssuer(ISSUER).build();
            DecodedJWT jwt = verifier.verify(token);
            tokenDTO.setIssuer(jwt.getIssuer());
            tokenDTO.setUsername(jwt.getClaim(CLAIM_USERNAME).asString());
            tokenDTO.setTokenType(jwt.getClaim(CLAIM_TOKEN_TYPE).asString());
            tokenDTO.setRole(jwt.getClaim(CLAIM_ROLE).asString());
            tokenDTO.setIssuedDate(jwt.getIssuedAt());
            tokenDTO.setExpiryDate(jwt.getExpiresAt());
            return tokenDTO;
        } catch (JWTVerificationException exception) {
            // Invalid signature/claims
            LOGGER.error("JWT", exception);
        } catch (Exception ex) {
            LOGGER.error("Has error in verify ADMIN SERVICE TOKEN.", ex);
        }
        return null;
    }
}
