package com.demo.hoidd.usermanagement.service;

import com.demo.hoidd.usermanagement.dto.JWTTokenDTO;
import com.demo.hoidd.usermanagement.dto.UserDTO;

public interface TokenService {
    String generate(UserDTO userDTO);

    JWTTokenDTO extractToken(String token);
}
