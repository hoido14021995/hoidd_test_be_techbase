package com.demo.hoidd.usermanagement.repository;

import com.demo.hoidd.usermanagement.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {
    Optional<UserEntity> findByUsername(@Param("login") String login);

    @Query("SELECT u FROM UserEntity u WHERE u.username = :name")
    UserEntity findUserExist(@Param("name") String name);

    @Query("SELECT u FROM UserEntity u WHERE u.teamID IN (:ids)")
    List<UserEntity> findUserEntityByTeamIDs(@Param("ids") List<Long> ids);

    @Query("SELECT u FROM UserEntity u WHERE u.teamID = :id")
    List<UserEntity> findUserEntityByTeamID(@Param("id") Long id);
}
