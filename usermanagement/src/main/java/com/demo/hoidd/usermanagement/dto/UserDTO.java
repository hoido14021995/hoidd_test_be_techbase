package com.demo.hoidd.usermanagement.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDTO implements Serializable {

    private static final long serialVersionUID = -6199826238880128930L;

    private Long id;

    private String username;

    private String password;

    private String role;

    private Long teamID;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String uername) {
        this.username = uername;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Long getTeamID() {
        return teamID;
    }

    public void setTeamID(Long teamID) {
        this.teamID = teamID;
    }

    public String getPassword() {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        if (password == null || password.trim().isEmpty()) {
            return passwordEncoder.encode("P@ssw0rd");
        }
        return password;

    }

    public void setPassword(String password) {
        this.password = password;
    }
}