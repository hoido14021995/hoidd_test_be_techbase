package com.demo.hoidd.usermanagement.utils;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.List;

public class KeyReader {
    private static final String RSA_INSTANCE = "RSA";

    /**
     * Get RSAPublicKey
     * 
     * @param filename
     * @return
     * @throws Exception
     */
    public static RSAPublicKey getPublicKey(String filename) throws Exception {
        byte[] keyBytes = Files.readAllBytes(Paths.get(filename));

        X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance(RSA_INSTANCE);
        return (RSAPublicKey) kf.generatePublic(spec);
    }

    /**
     * Get RSAPrivateKey
     * 
     * @param filename
     * @return
     * @throws Exception
     */
    public static RSAPrivateKey getPrivateKey(String filename) throws Exception {
        byte[] keyBytes = Files.readAllBytes(Paths.get(filename));

        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance(RSA_INSTANCE);
        return (RSAPrivateKey) kf.generatePrivate(spec);
    }

    public static boolean isNotNullOrEmpty(Object object) {
        return !isNullOrEmpty(object);
    }

    public static boolean isNullOrEmpty(Object object) {
        boolean result = false;
        if (object == null) {
            return true;
        }

        if (object instanceof String) {
            if (((String) object).equalsIgnoreCase("")) {
                result = true;
            }
        }

        if (object instanceof List<?>) {
            if (((List<?>) object).size() == 0) {
                result = true;
            }
        }

        return result;
    }
}
