package com.demo.hoidd.usermanagement.controller;

import com.demo.hoidd.usermanagement.dto.AuthenticationResponseDTO;
import com.demo.hoidd.usermanagement.dto.JWTTokenDTO;
import com.demo.hoidd.usermanagement.dto.LoginDTO;
import com.demo.hoidd.usermanagement.dto.UserDTO;
import com.demo.hoidd.usermanagement.service.TokenService;
import com.demo.hoidd.usermanagement.service.UserService;
import com.demo.hoidd.usermanagement.utils.KeyReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UserController {

    private static Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService mUserService;

    @Autowired
    private TokenService mTokenService;

    @PostMapping("/authenticate")
    public ResponseEntity authenticate(@Valid @RequestBody LoginDTO loginDTO) {
        LOGGER.info("authenticate start: {}", loginDTO.getUsername());
        AuthenticationResponseDTO authenticate = new AuthenticationResponseDTO();
        authenticate = mUserService.loginLocal(authenticate ,loginDTO);
        if (authenticate.hasError()) {
            LOGGER.error("authenticate failed: {}", loginDTO.getUsername());
        }
        return new ResponseEntity(authenticate, authenticate.getHttpStatus());
    }

    @GetMapping("/get-list-user")
    public ResponseEntity getListUser(@RequestHeader(value = "authorization", required = false) String authorization) {
        LOGGER.info("getListUser start:");
        JWTTokenDTO jwtTokenDTO = mTokenService.extractToken(authorization.split(" ")[1]);
        List<UserDTO> userDTOS = new ArrayList<>();
        if (KeyReader.isNotNullOrEmpty(jwtTokenDTO)) {
            String username = jwtTokenDTO.getUsername();
            String role = jwtTokenDTO.getRole();
            userDTOS = mUserService.getListUserByRole(role, username);
            return new ResponseEntity(userDTOS, HttpStatus.OK);
        }
        LOGGER.error("don't have any permission to get list data user");
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }
}
