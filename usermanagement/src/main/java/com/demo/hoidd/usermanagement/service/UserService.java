package com.demo.hoidd.usermanagement.service;

import com.demo.hoidd.usermanagement.dto.AuthenticationResponseDTO;
import com.demo.hoidd.usermanagement.dto.LoginDTO;
import com.demo.hoidd.usermanagement.dto.UserDTO;

import java.util.List;

public interface UserService {
    AuthenticationResponseDTO loginLocal(AuthenticationResponseDTO authenticationResponseDTO, LoginDTO loginDto);

    List<UserDTO> getListUserByRole(String role, String userName);
}
