package com.demo.hoidd.usermanagement.controller;

import com.demo.hoidd.usermanagement.dto.AuthenticationResponseDTO;
import com.demo.hoidd.usermanagement.dto.LoginDTO;
import com.demo.hoidd.usermanagement.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class LoginController {

    private static Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private UserService mUserService;

    @PostMapping("/authenticate")
    public ResponseEntity authenticate(@Valid @RequestBody LoginDTO loginDTO) {
        LOGGER.info("authenticate start: {}", loginDTO.getUsername());
        AuthenticationResponseDTO authenticate = new AuthenticationResponseDTO();
        authenticate = mUserService.loginLocal(authenticate ,loginDTO);
        if (authenticate.hasError()) {
            LOGGER.error("authenticate failed: {}", loginDTO.getUsername());
        }
        return new ResponseEntity(authenticate, authenticate.getHttpStatus());
    }
}
