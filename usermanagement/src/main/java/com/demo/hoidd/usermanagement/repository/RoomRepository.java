package com.demo.hoidd.usermanagement.repository;

import com.demo.hoidd.usermanagement.entity.RoomEntity;
import com.demo.hoidd.usermanagement.entity.TeamEntity;
import com.demo.hoidd.usermanagement.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface RoomRepository extends JpaRepository<RoomEntity, Long> {

    @Query("SELECT u FROM RoomEntity u WHERE u.roomManagerID = :id")
    RoomEntity findRoomByIDManager(@Param("id") Long id);


}
