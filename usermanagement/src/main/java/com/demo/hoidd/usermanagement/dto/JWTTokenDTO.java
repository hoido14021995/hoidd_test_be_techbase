package com.demo.hoidd.usermanagement.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class JWTTokenDTO implements Serializable {

    private static final long serialVersionUID = 516636643423571335L;

    @JsonProperty(value = "tokenType")
    private String mTokenType;

    @JsonProperty(value = "token")
    private String mToken;

    @JsonProperty(value = "username")
    private String mUsername;

    @JsonProperty(value = "role")
    private String mRole;

    @JsonProperty(value = "issuer")
    private String mIssuer;

    @JsonProperty(value = "issuedDate")
    private Date mIssuedDate;

    @JsonProperty(value = "expiryDate")
    private Date mExpiryDate;

    public String getTokenType() {
        return mTokenType;
    }

    public void setTokenType(String tokenType) {
        mTokenType = tokenType;
    }

    /**
     * @return the mToken
     */
    public String getToken() {
        return mToken;
    }

    /**
     * @param mToken
     *            the mToken to set
     */
    public void setToken(String mToken) {
        this.mToken = mToken;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

    /**
     * @return the mIssuer
     */
    public String getIssuer() {
        return mIssuer;
    }

    /**
     * @param mIssuer
     *            the mIssuer to set
     */
    public void setIssuer(String mIssuer) {
        this.mIssuer = mIssuer;
    }

    /**
     * @return the mIssuedDate
     */
    public Date getIssuedDate() {
        return mIssuedDate;
    }

    /**
     * @param mIssuedDate
     *            the mIssuedDate to set
     */
    public void setIssuedDate(Date mIssuedDate) {
        this.mIssuedDate = mIssuedDate;
    }

    /**
     * @return the mExpiryDate
     */
    public Date getExpiryDate() {
        return mExpiryDate;
    }

    /**
     * @param mExpiryDate
     *            the mExpiryDate to set
     */
    public void setExpiryDate(Date mExpiryDate) {
        this.mExpiryDate = mExpiryDate;
    }

    public String getRole() {
        return mRole;
    }

    public void setRole(String role) {
        mRole = role;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }
}
