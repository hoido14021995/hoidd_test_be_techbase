package com.demo.hoidd.usermanagement.entity;

import javax.persistence.*;

@Entity()
@Table(name = "room_info")
public class RoomEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name", columnDefinition = "VARCHAR(255)", nullable = true)
    private String name;

    @Column(name = "roomManagerID")
    private Long roomManagerID;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getRoomManagerID() {
        return roomManagerID;
    }

    public void setRoomManagerID(Long roomManagerID) {
        this.roomManagerID = roomManagerID;
    }
}
