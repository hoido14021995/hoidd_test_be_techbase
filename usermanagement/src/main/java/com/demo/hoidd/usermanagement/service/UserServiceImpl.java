package com.demo.hoidd.usermanagement.service;

import com.demo.hoidd.usermanagement.dto.AuthenticationResponseDTO;
import com.demo.hoidd.usermanagement.dto.LoginDTO;
import com.demo.hoidd.usermanagement.dto.UserDTO;
import com.demo.hoidd.usermanagement.entity.RoomEntity;
import com.demo.hoidd.usermanagement.entity.TeamEntity;
import com.demo.hoidd.usermanagement.entity.UserEntity;
import com.demo.hoidd.usermanagement.repository.RoomRepository;
import com.demo.hoidd.usermanagement.repository.TeamRepository;
import com.demo.hoidd.usermanagement.repository.UserRepository;
import com.demo.hoidd.usermanagement.utils.KeyReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private TokenService mTokenService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private TeamRepository teamRepository;

    @Override
    public AuthenticationResponseDTO loginLocal(AuthenticationResponseDTO authenticationResponseDTO, LoginDTO loginDto) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword()));
        authenticationResponseDTO.setAuthorities(authentication.getAuthorities());
        // Generating token
        authenticationResponseDTO = generatingToken(authenticationResponseDTO, loginDto);
        return authenticationResponseDTO;
    }


    private AuthenticationResponseDTO generatingToken(AuthenticationResponseDTO authenticationResponseDTO,  LoginDTO loginDto) {
        UserDTO userDTO = new UserDTO();
        userDTO.setUsername(loginDto.getUsername());
        authenticationResponseDTO.setToken(mTokenService.generate(userDTO));
        return authenticationResponseDTO;
    }

    @Override
    public List<UserDTO> getListUserByRole (String role, String userName) {
        List<UserEntity> listUser = new ArrayList<>();
        List<UserDTO> userDTOS = new ArrayList<>();
        switch (role) {
            case "GD":
                listUser = userRepository.findAll();
                break;
            case "TP":
                UserEntity tP = userRepository.findByUsername(userName).get();
                RoomEntity roomEntity = roomRepository.findRoomByIDManager(tP.getId());
                List<TeamEntity> teamEntities = teamRepository.findListTeamInRoom(roomEntity.getId());
                List<Long> teamId = teamEntities.stream().map(TeamEntity::getId)
                    .collect(Collectors.toList());
                listUser = userRepository.findUserEntityByTeamIDs(teamId);
            case "NV":
                UserEntity userEntity = userRepository.findUserExist(userName);
                Long currentTeamId = userEntity.getTeamID();
                listUser = userRepository.findUserEntityByTeamID(currentTeamId);
        }
        if (KeyReader.isNotNullOrEmpty(listUser)) {
            for (UserEntity userEntity : listUser) {
                UserDTO userDTO = new UserDTO();
                userDTO.setUsername(userEntity.getUsername());
                userDTO.setRole(userEntity.getRole());
                userDTOS.add(userDTO);
            }
        }
        return userDTOS;
    }
}
