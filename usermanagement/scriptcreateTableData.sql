CREATE TABLE user_info (
	user_name varchar(50) NOT NULL,
	password varchar(100) NOT NULL,
	role varchar(25),
	id bigint identity
    primary key,
	team_id bigint FOREIGN KEY REFERENCES team_info(id)
);

CREATE TABLE room_info (
	name varchar(100) NOT NULL,
	id bigint identity
    primary key,
	room_manager_id bigint
);

CREATE TABLE team_info (
	name varchar(100) NOT NULL,
	id bigint identity
    primary key,
	room_id bigint FOREIGN KEY REFERENCES room_info(id),
);